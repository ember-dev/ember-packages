#!/usr/bin/env python
#
#    #######################################################################
#
#    Author Josh.5  https://github.com/josh5
#
#    ########################################################################

## Package Specific Variables
PACKAGE_NAME    = 'CouchPotatoServer'
PACKAGE_CONFIG  = 'couchpotatoserver.ini'
PACKAGE_BIN     = ''



import os, subprocess, time

## Universal Variables
home          = os.path.expanduser('~/')
path          = home+"Programs/"
path_programs = home+"Programs/"
path_config   = home+"Configs/"+PACKAGE_NAME+"/"
path_defconf  = home+"Programs/"+PACKAGE_NAME+"/default-config/"
path_init     = home+"Run/"
path_bin      = path+"bin/"
path_pylib    = path+"pylib/"
program_conf  = path_config+PACKAGE_CONFIG

## Define Some Universal Functions
def ensure_dir(f):
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)



###################
###             ###
##  ! EXECUTE !  ##
###             ###
###################

# Create any configurations if required
if PACKAGE_CONFIG:
    ensure_dir(path_config)
    if os.path.isdir(path_defconf) and not PACKAGE_CONFIG in os.listdir(path_config):
        subprocess.Popen("cp -f "+path_defconf+'* '+path_config, shell=True, close_fds=True)

# Make stuff executable
subprocess.Popen("chmod -R +x "+path_init+"/*" , shell=True, close_fds=True)
subprocess.Popen("chmod -R +x "+path_bin+"/*" , shell=True, close_fds=True)

# Restart not start incase it's already running
subprocess.Popen(path_init+'P'+PACKAGE_NAME+' restart', shell=True, close_fds=True)

