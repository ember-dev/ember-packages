#!/usr/bin/env python
#
#    #######################################################################
#
#    Author Josh.5  https://github.com/josh5
#
#    ########################################################################

## Package Specific Variables
PACKAGE_NAME    = 'OpenVPN'
PACKAGE_CONFIG  = ''
PACKAGE_BIN     = 'openvpn'



import os, subprocess, time

## Universal Variables
home          = os.path.expanduser('~/')
path          = home+"Programs/"
path_programs = home+"Programs/"
path_config   = home+"Configs/"+PACKAGE_NAME+"/"
path_defconf  = home+"Programs/"+PACKAGE_NAME+"/default-config/"
path_init     = home+"Run/"
path_bin      = path+"bin/"
path_pylib    = path+"pylib/"
program_conf  = path_config+PACKAGE_CONFIG

## Define Some Universal Functions
def ensure_dir(f):
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)



###################
###             ###
##  ! EXECUTE !  ##
###             ###
###################

# Make stuff executable
subprocess.Popen("chmod -R +x "+path_init+"/*" , shell=True, close_fds=True)
subprocess.Popen("chmod -R +x "+path_bin+"/*" , shell=True, close_fds=True)

# Stop the process if init file exists
subprocess.Popen(path_init+'P'+PACKAGE_NAME+' stop', shell=True, close_fds=True)
time.sleep(5)

# Remove package files
if PACKAGE_NAME:
    subprocess.Popen('rm -Rf '+path+PACKAGE_NAME, shell=True, close_fds=True)		# <-- lockfile
    subprocess.Popen('rm -Rf '+path_init+'P'+PACKAGE_NAME, shell=True, close_fds=True)	# <-- init script
    subprocess.Popen('rm -Rf '+path_programs+PACKAGE_NAME, shell=True, close_fds=True)	# <-- program files
if PACKAGE_BIN:
    subprocess.Popen('rm -Rf '+path+"bin/"+PACKAGE_BIN, shell=True, close_fds=True)	# <-- bin executable

