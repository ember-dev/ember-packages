################################################################################
#
# ember_python_libs
#
################################################################################

EMBER_PYTHON_LIBS_VERSION = 0.0.5
EMBER_PYTHON_LIBS_SITE = $(BR2_EXTERNAL)/package/ember_python_libs/src
EMBER_PYTHON_LIBS_SITE_METHOD = local
EMBER_PYTHON_LIBS_INSTALL_STAGING = NO
EMBER_PYTHON_LIBS_INSTALL_TARGET = YES

EMBER_PYTHON_LIBS_DEPENDENCIES += python-configobj python-mako python-cheetah
EMBER_PYTHON_LIBS_PK_NAME = "PythonLibs"
EMBER_PYTHON_LIBS_PK_VERSION = $(EMBER_PYTHON_LIBS_VERSION)
EMBER_PYTHON_LIBS_PK_DESCRIPTION = "A collection of commonly used libraries and Python modules."
EMBER_PYTHON_LIBS_PK_DEPENDENCIES = ""
EMBER_PYTHON_LIBS_PK_REQUIREDFW = "2.0.0"
EMBER_PYTHON_LIBS_PK_DOWNLOAD = "https://bitbucket.org/ember-dev/ember-packages/raw/master/PACKAGES-armv7l/package-PythonLibs.tar.gz"



ifeq ($(BR2_ARCH),"arm")
PACKAGE_ARCH = armv7l
else
PACKAGE_ARCH = $(BR2_ARCH)
endif
OUTPUTDIR = $(BASE_DIR)/../../PACKAGES-$(PACKAGE_ARCH)

define EMBER_PYTHON_LIBS_INSTALL_TARGET_CMDS
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Programs/$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))
	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Run

	echo '[$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))]' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Programs/$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/package_info
	echo 'name = $(EMBER_PYTHON_LIBS_PK_NAME)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Programs/$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/package_info
	echo 'description = $(EMBER_PYTHON_LIBS_PK_DESCRIPTION)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Programs/$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/package_info
	echo 'version = "$(EMBER_PYTHON_LIBS_PK_VERSION)"' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Programs/$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/package_info
	echo 'dependencies = $(EMBER_PYTHON_LIBS_PK_DEPENDENCIES)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Programs/$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/package_info
	echo 'requiredFW = $(EMBER_PYTHON_LIBS_PK_REQUIREDFW)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Programs/$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/package_info
	echo 'download = $(EMBER_PYTHON_LIBS_PK_DOWNLOAD)' >> $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Programs/$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/package_info

	mkdir -p $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Programs/pylib
	cp -rfv $(TARGET_DIR)/usr/lib/python2.7/site-packages/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Programs/pylib
	cp -rfv $(BR2_EXTERNAL)/package/ember_python_libs/src/* $(OUTPUTDIR)/package-$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))/Programs/$(call qstrip,$(EMBER_PYTHON_LIBS_PK_NAME))
endef

$(eval $(generic-package))
